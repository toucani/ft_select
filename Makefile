# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/05/11 20:28:25 by dkovalch          #+#    #+#              #
#    Updated: 2017/10/31 21:13:37 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

								#################
								#	VARIABLES	#
								#################

NAME			= ft_select
NAME_D			= ft_select_deb

CC				= gcc

STD_FLAGS		= -x c -std=gnu11 -D _REENTRANT
ERR_FLAGS		= -Wall -Werror -Wextra -Wstrict-prototypes

COMPILE_FLAGS	= $(STD_FLAGS) $(ERR_FLAGS) -O3 -D NDEBUG
COMPILE_FLAGS_D	= $(STD_FLAGS) $(ERR_FLAGS) -O0 -D DEBUG -g -g3 $(SANITY_FLAGS)

LINKING_FLAGS	= $(ERR_FLAGS) -O3
LINKING_FLAGS_D	= $(ERR_FLAGS) -O0 -g -g3 $(SANITY_FLAGS)

LIBRARIES		= $(FT_PRINTF) $(LIBFT) -ltermcap
LIBRARIES_D		= $(FT_PRINTF_D) $(LIBFT_D) -ltermcap

SANITY_FLAGS	= -fno-omit-frame-pointer \
					-fsanitize=address,shift,null,signed-integer-overflow \
					-fsanitize=vla-bound,bool,enum,undefined

GCC7			:= $(shell command -v gcc-7 2> /dev/null)
ifdef GCC7
CC				= gcc-7
ERR_FLAGS		+= -Wnull-dereference
SANITY_FLAGS	+= -fsanitize=leak,bounds-strict
endif

INCLUDES		= -iquote includes -iquote $(FT_PRINTF_FLD) -iquote $(LIBFT_FLD)

PRINTF_2_ARGS	= @printf "%-15s %-65s\n"
PRINTF_3_ARGS	= @printf "%-15s %-65s%20s\n"


								#################
								#	LIBRARIES	#
								#################

LIBRARIES_FLD	= libraries

LIBFT_FLD		= $(LIBRARIES_FLD)/libft

LIBFT			= $(LIBFT_FLD)/libft.a
LIBFT_D			= $(LIBFT_FLD)/libftD.a

FT_PRINTF_FLD	= $(LIBRARIES_FLD)/ft_printf

FT_PRINTF		= $(FT_PRINTF_FLD)/libftprintf.a
FT_PRINTF_D		= $(FT_PRINTF_FLD)/libftprintfD.a


								#################
								#	SOURCES		#
								#################

VPATH	= sources:sources/controller:sources/model:sources/view


								#################
								#	OBJECTS		#
								#################

OBJECTS_FLD = objects

OBJECTS =\
	$(OBJECTS_FLD)/main.o\
	$(OBJECTS_FLD)/init.o\
	$(OBJECTS_FLD)/loop.o\
	$(OBJECTS_FLD)/model.o\
	$(OBJECTS_FLD)/view.o\
	$(OBJECTS_FLD)/draw.o\
	$(OBJECTS_FLD)/start_finder.o\
	$(OBJECTS_FLD)/termcap.o\
	$(OBJECTS_FLD)/controller.o

OBJECTS_D = $(patsubst %.o, %_D.o, $(OBJECTS))


								#############
								#	RULES	#
								#############

.SILENT :

all : $(NAME)

debug : $(NAME_D)

$(OBJECTS_FLD) :
	mkdir -p $(OBJECTS_FLD)

$(FT_PRINTF) :
	$(PRINTF_2_ARGS) "M͡ak̕i̵ng" "ft_printf"
	make -j -C $(FT_PRINTF_FLD)

$(LIBFT) :
	$(PRINTF_2_ARGS) "M͡ak̕i̵ng" "libft"
	make -j -C $(LIBFT_FLD)

$(FT_PRINTF_D) :
	$(PRINTF_2_ARGS) "M͡ak̕i̵ng" "ft_printf debug"
	make debug -j -C $(FT_PRINTF_FLD)

$(LIBFT_D) :
	$(PRINTF_2_ARGS) "M͡ak̕i̵ng" "libft debug"
	make debug -j -C $(LIBFT_FLD)

clean :
	rm -rf $(OBJECTS_FLD)
	make clean -C $(FT_PRINTF_FLD)
	make clean -C $(LIBFT_FLD)

fclean : clean
	rm -rf $(NAME) $(NAME_D) $(NAME).dSym $(NAME_D).dSym
	make fclean -C $(FT_PRINTF_FLD)
	make fclean -C $(LIBFT_FLD)

re : fclean all

dre : fclean debug

	#########################
	#	compilation rules	#
	#########################

$(NAME) : $(LIBFT) $(FT_PRINTF) $(OBJECTS_FLD) $(OBJECTS)
	$(PRINTF_2_ARGS) "Li͢nki͞n҉g" $(NAME)
	$(CC) $(INCLUDES) $(LINKING_FLAGS) $(OBJECTS) $(LIBRARIES) -o $(NAME)

$(NAME_D) : $(LIBFT_D) $(FT_PRINTF_D) $(OBJECTS_FLD) $(OBJECTS_D)
	$(PRINTF_3_ARGS) "Li͢nki͞n҉g" $(NAME_D) "wit͠h̀ ̨śa̵nit̷iz̡eŕs̷"
	$(CC) $(INCLUDES) $(LINKING_FLAGS_D) $(OBJECTS_D) $(LIBRARIES_D) -o $(NAME_D)

	#####################
	#	objects rules	#
	#####################

$(OBJECTS_FLD)/%.o : %.c
	$(PRINTF_2_ARGS) "Co̸m͢p͏ili͢ng" $<
	$(CC) $(INCLUDES) $(COMPILE_FLAGS) -c $< -o $@

$(OBJECTS_FLD)/%_D.o : %.c
	$(PRINTF_3_ARGS) "Co̸m͢p͏ili͢ng" $< "with sanitizers"
	$(CC) $(INCLUDES) $(COMPILE_FLAGS_D) -c $< -o $@
