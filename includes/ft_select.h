/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 21:00:55 by dkovalch          #+#    #+#             */
/*   Updated: 2017/11/02 22:52:29 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_select project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FT_SELECT_H
# define FT_SELECT_H

# include <stdbool.h>
# include <termios.h>

/*
**    Screen position structure.
**    The coordinates can be invalid: -1.
*/

typedef struct          s_position
{
    int                 line;
    int                 col;
}                       t_position;

/*
**    Main model structure.
**    line        ->    the string argument we got
**    is_selected    ->    is selected to go to output?
**    is_focused    ->    is "cursor" above this element?
**    position    ->    current screen position, is used in drawing
*/

typedef struct          s_choice
{
    const char *const   line;
    bool                is_selected;
    bool                is_focused;
    t_position          position;
    struct s_choice     *next;
    struct s_choice     *prev;
}                       t_choice;


/*
**    Main structure
**    This structure holds the info we to need, and result of on-screen drawing.
**    Also it holds the list of choices.
*/

typedef struct          s_info
{
    bool                is_on_screen;
    unsigned            biggest_width;
    unsigned            choices_am;
    const int           terminal_fd;
    struct termios      terminal_origin;
    struct termios      terminal_mine;
    t_choice           *choices;
}                       t_info;

extern t_info           g_info;

#endif
