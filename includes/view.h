/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   view.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/15 21:28:32 by dkovalch          #+#    #+#             */
/*   Updated: 2017/11/02 22:51:28 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_select project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIEW_H
# define VIEW_H

# include "ft_select.h"

# define PRINT_ARROW_LEFT        "<--"
# define PRINT_ARROW_RGHT        "-->"
# define PRINT_LAST_LINE_OFF    \
    MAX(ft_strlen(PRINT_ARROW_LEFT), ft_strlen(PRINT_ARROW_RGHT))

# define PRINT_FOCUSED_LEFT        "[ "
# define PRINT_FOCUSED_RGHT        " ]"
# define PRINT_UNFOCUSED_LEFT    "  "
# define PRINT_UNFOCUSED_RGHT    "  "
# define PRINT_X_OFF            MAX(\
    MAX(ft_strlen(PRINT_FOCUSED_LEFT), ft_strlen(PRINT_FOCUSED_RGHT)), \
    MAX(ft_strlen(PRINT_UNFOCUSED_LEFT), ft_strlen(PRINT_UNFOCUSED_RGHT)))

void        redraw(void);
void        clear_positions(void);
t_choice    *get_right_start(const unsigned total_choices_per_screen);
void        do_draw(const struct winsize *size);
void        redraw_choice(const t_choice *const choice);

void        screen_erase(void);
void        set_cursor_position(const unsigned col, const unsigned line);
void        bell(void);

/*
**    This is a termcap putchar.
*/

int         t_putchar(int ch);

#endif
