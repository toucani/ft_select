/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 20:54:55 by dkovalch          #+#    #+#             */
/*   Updated: 2017/11/02 22:53:06 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_select project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "model.h"
#include "ft_printf.h"
#include "ft_select.h"
#include "controller.h"

t_info    g_info;

/*
**    Main
**    This functions prints simple help message.
*/

static void    print_help(void)
{
    ft_printf("%r%-40s%40s%r\n\n", "bold", "FT_SELECT", "FT_SELECT", "reset");
    ft_printf("%r%s%r\n\t", "bold", "NAME:", "reset");
    ft_printf("ft_select - graphical arguments selector.\n\n");
    ft_printf("%r%s%r\n\t", "bold", "SYNOPSIS:", "reset");
    ft_printf("ft_select [%rarguments%r ...]\n\n", "_", "reset");
    ft_printf("%r%s%r\n\t", "bold", "DESCRIPTION:", "reset");
    ft_printf("Ft_select allows you to specify what %rarguments%r to"
    " give back.\n\tThis simple tool gets %rarguments%r as strings, and"
    " returnes\n\tselected ones to stdout. It is useful if you want to take control of\n"
    "\tthe arguments one command passes to another, or have the ability\n\t"
    "to exclude some arguments before the command is started. You can\n\trun %r"
    "ft_select `ls` | xargs echo%r to see the result.\n\n", "_", "reset", "_",
    "reset", "_", "reset");
    ft_printf("%r%s%r\n", "bold", "USAGE:", "reset");
    ft_putendl("\tArrow up\t- navigate thru the list");
    ft_putendl("\tArrow down\t- navigate thru the list");
    ft_putendl("\tBackspace\t- remove current item");
    ft_putendl("\tDelete\t\t- remove current item");
    ft_putendl("\tSpace\t\t- select or deselect the item");
    ft_putendl("\tEscape\t\t- exit without printing anything to STDOUT");
    ft_putendl("\tEnter\t\t- exit and print selected items to STDOUT\n");
    ft_printf("%r%s%r\n", "bold", "LICENSE:", "reset");
    ft_putendl("\tft_select - Copyright (C) 2017, Dmytro Kovalchuk.\n");
    ft_putendl("\tThis program is licensed under GNU GPL v3 and");
    ft_putendl("\tcomes with ABSOLUTELY NO WARRANTY. This is free software,");
    ft_putendl("\tand you are welcome to redistribute it under certain");
    ft_putendl("\tconditions; see the LICENCE.md for full information.\n");
    ft_printf("%-28s     %-19s%28s\n\n", "LSD", "Version 2.1", "LSD");
}

/*
**    Main
**    This is MAAAIIIIIIIIN.
*/

int            main(int ac, const char *const *const av)
{
    bool    result;

    if (ac > 1 && ft_strequ(av[1], "--help"))
    {
        print_help();
        exit(EXIT_SUCCESS);
    }
    else if (ac == 1)
        exit(EXIT_SUCCESS);
    ft_bzero(&g_info, sizeof(t_info));
    g_info.choices = import_choices(&(av[1]));
    g_info.choices->is_focused = true;
    update_choices_width();
    init_terminal();
    grab_terminal();
    result = run_loop();
    release_terminal();
    if (result)
        export_choices(g_info.choices);
}
