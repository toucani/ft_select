/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/15 20:09:40 by dkovalch          #+#    #+#             */
/*   Updated: 2017/11/02 22:52:54 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_select project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "view.h"
#include "libft.h"
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <termcap.h>
#include "ft_select.h"
#include "controller.h"

/*
**    Signal handlers
*/

static void    grab(const int signo)
{
    if (signo > 0)
    {
        grab_terminal();
    }
}

static void    release(const int signo)
{
    if (signo > 0)
    {
        release_terminal();
        raise(SIGSTOP);
    }
}

static void    quit(const int signo)
{
    if (signo > 0)
    {
        release_terminal();
        exit(signo);
    }
}

static void    winsize_changed(const int signo)
{
    if (signo == SIGWINCH)
        redraw();
}

/*
**    Controlller
**    This function sets right disposition for signals.
**    We intersept as many signals as possible, to be able to restore terminal
**    to its origin settings when the programm is killed or stopped.
*/

static void    init_signals(void)
{
    struct sigaction    signals;

    ft_bzero((void*)&signals, sizeof(struct sigaction));
    sigemptyset(&(signals.sa_mask));
    signals.sa_handler = quit;
    if (sigaction(SIGABRT, &signals, NULL) != 0
    || sigaction(SIGINT, &signals, NULL) != 0
    || sigaction(SIGQUIT, &signals, NULL) != 0
    || sigaction(SIGHUP, &signals, NULL) != 0
    || sigaction(SIGTERM, &signals, NULL) != 0)
        print_fatal_error("error registering signals.");
    signals.sa_flags = SA_RESTART;
    signals.sa_handler = release;
    if (sigaction(SIGTSTP, &signals, NULL) != 0
    || sigaction(SIGTTIN, &signals, NULL) != 0
    || sigaction(SIGTTOU, &signals, NULL) != 0)
        print_fatal_error("error registering signals.");
    signals.sa_handler = grab;
    if (sigaction(SIGCONT, &signals, NULL) != 0)
        print_fatal_error("error registering signals.");
    signals.sa_handler = winsize_changed;
    if (sigaction(SIGWINCH, &signals, NULL) != 0)
        print_fatal_error("error registering SIGWINCH.");
}

/*
**    Controlller
**    This function initializes termcaps.
*/

static void    init_termcap(void)
{
    if (tgetent(NULL, NULL) < 1)
        print_fatal_error("error initialization termcaps: tgetent failure.");
    if (tgetstr("cm", NULL) == NULL || tgetstr("ho", NULL) == NULL
    || tgetstr("bl", NULL) == NULL
    || tgetstr("cl", NULL) == NULL || tgetstr("ve", NULL) == NULL
    || tgetstr("us", NULL) == NULL || tgetstr("ue", NULL) == NULL
    || tgetstr("mr", NULL) == NULL || tgetstr("me", NULL) == NULL
    || tgetstr("ti", NULL) == NULL || tgetstr("te", NULL) == NULL)//Add invisible stuff
        print_fatal_error("missing terminal capability...");
    PC = tgetstr("pc", NULL) ? tgetstr("pc", NULL)[0] : '\0';
}

/*
**    Controlller
**    This function opens and initializes terminal.
*/

void        init_terminal(void)
{
    *((int *)&(g_info.terminal_fd)) = open("/dev/tty", O_RDWR);
    if (g_info.terminal_fd < 0 || !isatty(g_info.terminal_fd))
        print_fatal_error("error opening terminal.");
    init_termcap();
    init_signals();
    tcgetattr(g_info.terminal_fd, &(g_info.terminal_origin));
    g_info.terminal_mine = g_info.terminal_origin;
    g_info.terminal_mine.c_cflag |= CLOCAL | CREAD | PARENB | CS8;
    g_info.terminal_mine.c_iflag |= INPCK | IGNPAR | IXOFF | IXON;
    g_info.terminal_mine.c_iflag &= ~(ISTRIP | BRKINT);
    g_info.terminal_mine.c_lflag &= ~(ECHO | ECHOE | ECHONL | ICANON | IEXTEN);
    g_info.terminal_mine.c_lflag |= ISIG | TOSTOP;
    g_info.terminal_mine.c_cc[VMIN] = 1;
    g_info.terminal_mine.c_cc[VTIME] = 0;
    ospeed = cfgetospeed(&(g_info.terminal_mine));
}
