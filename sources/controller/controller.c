/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controller.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/15 20:09:40 by dkovalch          #+#    #+#             */
/*   Updated: 2017/11/02 22:54:07 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_select project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "view.h"
#include "libft.h"
#include <unistd.h>
#include <termcap.h>
#include "ft_select.h"

/*
**    Controlller
**    This function lokks for the widest line among choices.
*/

void    update_choices_width(void)
{
    const t_choice    *temp;

    temp = g_info.choices;
    while (temp != NULL)
    {
        g_info.biggest_width = MAX(g_info.biggest_width, ft_strlen(temp->line));
        temp = temp->next;
    }
}

/*
**    Controlller
**    This function sets terminal to "free drawing" mode.
*/

void    grab_terminal(void)
{
    tcsetattr(g_info.terminal_fd, TCSANOW, &(g_info.terminal_mine));
    tputs(tgetstr("ti", NULL), 1, t_putchar);
    tputs(tgetstr("vi", NULL), 1, t_putchar);
    redraw();
}

/*
**    Controlller
**    This function restores original terminal state.
*/

void    release_terminal(void)
{
    tputs(tgetstr("ve", NULL), 1, t_putchar);
    tputs(tgetstr("te", NULL), 1, t_putchar);
    tcsetattr(g_info.terminal_fd, TCSANOW, &(g_info.terminal_origin));
}

/*
**    Controlller
**    This function prints error, and exits.
*/

void    print_fatal_error(const char *const msg)
{
    ft_putstr_fd("ft_select: ", STDERR_FILENO);
    ft_putstr_fd(msg, STDERR_FILENO);
    ft_putstr_fd("\n", STDERR_FILENO);
    exit(EXIT_FAILURE);
}
