/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 22:24:29 by dkovalch          #+#    #+#             */
/*   Updated: 2017/11/02 22:54:34 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_select project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "view.h"
#include "model.h"
#include "libft.h"
#include "ft_select.h"
#include "controller.h"

# define KEY_ESCAPE    "\x1B"
# define KEY_SPACE    "\x20"
# define KEY_RETURN    "\x0a"
# define KEY_BACKSP    "\x7f"
# define KEY_DELETE    "\x1b\x5b\x33\x7e"
# define KEY_ARR_UP    "\x1b\x5b\x41"
# define KEY_ARR_DW    "\x1b\x5b\x42"

/*
**    Controlller
**    This function returns previous choice.
*/

static t_choice    *get_prev(t_choice *choice)
{
    t_choice    *temp;

    if (choice == NULL)
        return (NULL);
    if (choice->prev != NULL)
        return (choice->prev);
    temp = choice;
    while (temp->next != NULL)
        temp = temp->next;
    return (temp);
}

/*
**    Controlller
**    This function returns next choice.
*/

static t_choice    *get_next(t_choice *choice)
{
    if (choice == NULL)
        return (NULL);
    return (choice->next ? choice->next : g_info.choices);
}

/*
**    Controlller
**    This function selects given choice and moves focus to the next element.
**    It returns next valid "current" choice, already focused and redrawn.
*/

static t_choice    *key_select(t_choice *const current)
{
    t_choice *const    rt = get_next(current);

    if (current != NULL)
    {
        toggle_selection(current);
        toggle_focus(current);
        redraw_choice(current);
    }
    if (rt != NULL)
    {
        toggle_focus(rt);
        redraw_choice(rt);
    }
    return (rt);
}

/*
**    Controlller
**    This function deletes given choice.
**    It returns next valid "current" choice, already focused and redrawn.
*/

static t_choice    *key_delete(t_choice *const current, const bool next_focus)
{
    t_choice    *rt = next_focus ? get_next(current) : get_prev(current);

    if (current != NULL)
        remove_choice(current);
    if (rt != NULL && rt != current)
    {
        toggle_focus(rt);
        update_choices_width();
    }
    else
        rt = NULL;
    redraw();
    return (rt);
}

/*
**    Controlller
**    This function moves focus from given choice to the next one.
**    It returns next valid "current" choice, already focused and redrawn.
*/

static t_choice    *key_move(t_choice *const current, const bool next)
{
    t_choice *const    rt = next ? get_next(current) : get_prev(current);

    if (current != NULL)
    {
        toggle_focus(current);
        redraw_choice(current);
    }
    if (rt != NULL)
    {
        toggle_focus(rt);
        redraw_choice(rt);
    }
    return (rt);
}

/*
**    Controlller
**    This function is main application loop. Reads the key and acts.
*/

bool               run_loop(void)
{
    char        key[9];
    t_choice    *current;

    ft_bzero((void*)key, 9);
    current = get_focused();
    while (read(g_info.terminal_fd, key, 8) > 0)
    {
        if (ft_strequ(key, KEY_ESCAPE))
            return (false);
        else if (ft_strequ(key, KEY_SPACE))
            current = key_select(current);
        else if (ft_strequ(key, KEY_RETURN))
            return (true);
        else if (ft_strequ(key, KEY_ARR_DW) || ft_strequ(key, KEY_ARR_UP))
            current = key_move(current, ft_strequ(key, KEY_ARR_DW));
        else if (ft_strequ(key, KEY_BACKSP) || ft_strequ(key, KEY_DELETE))
            current = key_delete(current, ft_strequ(key, KEY_DELETE));
        else
            bell();
        ft_strclr(key);
    }
    return (false);
}
