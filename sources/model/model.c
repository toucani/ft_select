/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   model.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/15 20:04:02 by dkovalch          #+#    #+#             */
/*   Updated: 2017/11/02 22:54:07 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_select project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "model.h"
#include "ft_select.h"

/*
**    Model management
**    This function creates the model with given string arguments as choices.
*/

t_choice    *import_choices(const char *const *const av)
{
    t_choice    *head;
    t_choice    *temp;

    g_info.choices_am = 0;
    temp = NULL;
    head = NULL;
    while (av[g_info.choices_am] != NULL)
    {
        if (temp == NULL)
        {
            temp = (t_choice*)ft_memalloc(sizeof(t_choice));
            head = temp;
        }
        else
        {
            temp->next = (t_choice*)ft_memalloc(sizeof(t_choice));
            temp->next->prev = temp;
            temp = temp->next;
        }
        *((const char**)(&(temp->line))) = av[g_info.choices_am];
        temp->position.line = temp->position.col = -1;
        g_info.choices_am++;
    }
    return (head);
}

/*
**    Model management
**    This function removes given choice and relinks the list.
*/

void        remove_choice(t_choice *choice)
{
    t_choice    *temp;

    temp = choice;
    if (choice == g_info.choices)
        g_info.choices = choice->next;
    if (choice->prev != NULL)
        choice->prev->next = choice->next;
    if (choice->next != NULL)
        choice->next->prev = choice->prev;
    ft_memdel((void**)(&temp));
    g_info.choices_am--;
}

/*
**    Model management
**    This function toggles given choice selection.
*/

void        toggle_selection(t_choice *choice)
{
    choice->is_selected = !choice->is_selected;
}

/*
**    Model management
**    This function toggles given choice focus.
*/

void        toggle_focus(t_choice *choice)
{
    choice->is_focused = !choice->is_focused;
}

/*
**    Model
**    This function returns selected choice, or the first one if none is selected.
*/

t_choice    *get_focused(void)
{
    t_choice *head = g_info.choices;

    while(head != NULL && !(head->is_focused))
        head = head->next;
    return (head != NULL ? head : g_info.choices);
}

/*
**    Model management
**    This function prints selected choices to STDOUT.
*/

void        export_choices(const t_choice *head)
{
    bool    need_space;

    need_space = false;
    while (head)
    {
        if (head->is_selected)
        {
            if (need_space)
                ft_putchar(' ');
            ft_putstr(head->line);
            need_space = true;
        }
        head = head->next;
    }
}
