/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 20:03:20 by dkovalch          #+#    #+#             */
/*   Updated: 2017/11/02 22:54:53 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_select project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "view.h"
#include "libft.h"
#include "model.h"
#include <termios.h>
#include "ft_select.h"

/*
**    View management
**    This function draws bottom line arrows.
*/

static void        draw_arrows(const struct winsize *const size)
{
    set_cursor_position(1, size->ws_row);
    ft_putstr_fd(PRINT_ARROW_LEFT , g_info.terminal_fd);
    set_cursor_position(size->ws_col - ft_strlen(PRINT_ARROW_RGHT) + 1, size->ws_row);
    ft_putstr_fd(PRINT_ARROW_RGHT, g_info.terminal_fd);
}

/*
**    View management
**    This function counts total number of pages we have.
*/

static unsigned    get_total_pages(const unsigned total_choices_per_screen)
{
    return ((g_info.choices_am / total_choices_per_screen)
        + ((g_info.choices_am % total_choices_per_screen) > 0 ? 1 : 0));
}

/*
**    View management
**    This function counts number of the page we're currently on.
*/

static unsigned    get_current_page_no(const unsigned total_choices_per_screen,
                    const t_choice *const start)
{
    unsigned    am = 0;
    t_choice    *temp;

    temp = g_info.choices;
    while (temp && temp != start)
    {
        temp = temp->next;
        am++;
    }
    return ((am / total_choices_per_screen) +
        ((am % total_choices_per_screen) > 0 ? 1 : 0) + 1);
}

/*
**    View management
**    This function draws bottom line with arrows, and numbers the pages.
*/

static void        draw_bottom_line(t_choice *start,
                    const unsigned total_choices_per_screen,
                    const unsigned printed_choices,
                    const struct winsize *const size)
{
    char            *pages = NULL;
    const unsigned    total_pages = get_total_pages(total_choices_per_screen);
    const unsigned    current_page = get_current_page_no(total_choices_per_screen, start);

    if (printed_choices < g_info.choices_am)
        draw_arrows(size);
    pages = ft_itoa(current_page);
    set_cursor_position(
        MAX(size->ws_col / 2, ft_strlen(pages)) - ft_strlen(pages),
        size->ws_row);
    ft_putstr_fd(pages, g_info.terminal_fd);
    ft_strdel(&pages);
    ft_putchar_fd('/', g_info.terminal_fd);
    pages = ft_itoa(total_pages);
    ft_putstr_fd(pages, g_info.terminal_fd);
    ft_strdel(&pages);
}

/*
**    View management
**    This function draws as many choices as it can, starting from the given one
**    an assigning all the drawn choices their on-screen position. It doesnt put
**    any choice to the bottom line, since we will have arrows and
**    total count there.
*/

static unsigned    put_choices(t_choice *choice, const struct winsize *const size,
    const unsigned real_col_width)
{
    unsigned    current_col = 0;
    unsigned    current_line = 1;
    unsigned    rt = 0;

    while (choice && real_col_width * (current_col + 1) <= size->ws_col)
    {
        if (current_line >= size->ws_row)
        {
            current_line = 1;
            current_col++;
        }
        else
        {
            choice->position.line = current_line;
            choice->position.col = real_col_width * current_col;
            redraw_choice(choice);
            choice = choice->next;
            current_line++;
            rt++;
        }
    }
    return (rt);
}

/*
**    View management
**    This function gathers necessary data and calls actual drawing functions.
*/

void               do_draw(const struct winsize *size)
{
    unsigned    real_col_width;
    unsigned    total_choices_per_screen;
    unsigned    printed_choices;
    t_choice    *start;

    real_col_width = g_info.biggest_width + (PRINT_X_OFF * 2) + 1;
    total_choices_per_screen = (size->ws_col / real_col_width) * (size->ws_row - 1);
    start = get_right_start(total_choices_per_screen);
    clear_positions();
    printed_choices = put_choices(start, size, real_col_width);
    draw_bottom_line(start, total_choices_per_screen, printed_choices, size);
}
