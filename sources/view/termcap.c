/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   termcap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/24 20:12:46 by dkovalch          #+#    #+#             */
/*   Updated: 2017/11/02 22:55:19 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_select project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include <termcap.h>
#include "ft_select.h"

/*
**    This is a termcap putchar.
*/

int    t_putchar(int ch)
{
    return (ft_putchar_fd(ch, g_info.terminal_fd));
}

/*
**    Clears the screen, ans sets cursor home.
*/

void    screen_erase(void)
{
    tputs(tgetstr("cl", NULL), tgetnum("li"), t_putchar);
}

/*
**    Puts cursor at exact, 1 based position.
*/

void    set_cursor_position(const unsigned col, const unsigned line)
{
    tputs(tgoto(tgetstr("cm", NULL), col - 1, line - 1),
        tgetnum("li"), t_putchar);
}

/*
**    This function rings a bell:)
*/

void    bell(void)
{
    tputs(tgetstr("bl", NULL), 1, t_putchar);
}
