/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   start_finder.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 21:13:21 by dkovalch          #+#    #+#             */
/*   Updated: 2017/11/02 22:55:10 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_select project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "model.h"
#include "ft_select.h"

/*
**    View management
**    This function counts distance between 2 choices.
**    Returns -1 if reaches end of the list, without finding given "last".
*/

static int                distance(const t_choice *first, const t_choice *last)
{
    int    rt = 0;

    while (first != NULL && first != last)
    {
        first = first->next;
        rt++;
    }
    return (first == last ? rt : -1);
}

/*
**    View management
**    This function returns the choice, which is amount times next,
**    if possible, or NULL.
*/

static const t_choice    *move_next(const t_choice *current, unsigned amount)
{
    while (current != NULL && amount--)
        current = current->next;
    return (current);
}


/*
**    View management
**    This function decides from which choice we start drawing just checkin onto
**    what "page" focused choice can be placed.
*/

t_choice                *get_right_start(const int total_choices_per_screen)
{
    const t_choice    *const focused = get_focused();
    const t_choice    *first_on_screen = NULL;

    first_on_screen = g_info.choices;
    while (distance(first_on_screen, focused) >= total_choices_per_screen)
        first_on_screen = move_next(first_on_screen, total_choices_per_screen);
    return ((t_choice*)first_on_screen);
}
