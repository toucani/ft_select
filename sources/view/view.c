/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   view.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 22:24:29 by dkovalch          #+#    #+#             */
/*   Updated: 2017/11/02 22:55:34 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_select project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "view.h"
#include "libft.h"
#include "model.h"
#include <termios.h>
#include <termcap.h>
#include "ft_select.h"
#include <sys/ioctl.h>

/*
**    View management
**    This function checks if we can fit at least one column on the screen.
**    We add 4 to last line offset since we have a line "1/2"
**    which is 3 and 2 spaces, before and after the arross.
**
**    Return values:
**        true - we can fit it
**        false - no way
*/

static bool    can_fit(const struct winsize *const size)
{
    return ((g_info.biggest_width + (PRINT_X_OFF * 2) + 1)
        <= MAX(size->ws_col, (PRINT_LAST_LINE_OFF * 2) + 4));
}

/*
**    View management
**    This function clears position structure of all choices we have,
**    to be sure that only choices actually on screen will have valid positions.
*/

void           clear_positions(void)
{
    t_choice    *temp = g_info.choices;

    while (temp != NULL)
    {
        temp->position.col = -1;
        temp->position.line = -1;
        temp = temp->next;
    }
}

/*
**    View management
**    This function prints a message in the center of the screen.
*/

static void    print_sorry(const struct winsize *const size)
{
    set_cursor_position(MAX(size->ws_col / 2, 2) - 2, size->ws_row / 2);
    ft_putstr_fd("Oops!", g_info.terminal_fd);
    set_cursor_position(MAX(size->ws_col / 2, 12) - 12,
        (size->ws_row / 2) + 1);
    ft_putstr_fd("The screen is too small.", g_info.terminal_fd);
}

/*
**    View management
**    This function redraws the screen. We must be in "free movement" mode.
*/

void           redraw(void)
{
    struct winsize    size;

    screen_erase();
    ioctl(g_info.terminal_fd, TIOCGWINSZ, &size);
    if ((g_info.is_on_screen = can_fit(&size)))
        do_draw(&size);
    else
    {
        clear_positions();
        print_sorry(&size);
    }
}

/*
**    View management
**    This function redraws given choice. We must be in "free movement" mode.
*/

void           redraw_choice(const t_choice *const choice)
{
    if (choice->position.col >= 0 && choice->position.line >= 0)
    {
        set_cursor_position(choice->position.col, choice->position.line);
        ft_putstr_fd(choice->is_focused
            ? PRINT_FOCUSED_LEFT : PRINT_UNFOCUSED_LEFT, g_info.terminal_fd);
        if (choice->is_focused)
            tputs(tgetstr("us", NULL), 1, t_putchar);
        if (choice->is_selected)
            tputs(tgetstr("mr", NULL), 1, t_putchar);
        ft_putstr_fd(choice->line, g_info.terminal_fd);
        if (choice->is_selected)
            tputs(tgetstr("me", NULL), 1, t_putchar);
        if (choice->is_focused)
            tputs(tgetstr("ue", NULL), 1, t_putchar);
        ft_putstr_fd(choice->is_focused
            ? PRINT_FOCUSED_RGHT : PRINT_UNFOCUSED_RGHT, g_info.terminal_fd);
    }
    else
        redraw();
}
